<?php

Yii::import('zii.widgets.CPortlet');

class UserMenu extends CPortlet
{
    public $menu;
	public function init()
	{
        $this->menu = Yii::app()->controller->menu;
		parent::init();
	}

	protected function renderContent()
	{
		    $this->render('userMenu');
	}
}