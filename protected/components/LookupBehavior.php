<?php
/**
 * Created by IntelliJ IDEA.
 * User: Anton Demin
 * Date: 30.10.13
 * Time: 22:05
 */

class LookupBehavior extends CActiveRecordBehavior
{

    private  $_items=array();
    private  $_list=array();
    public $id_field = 'Id';
    public $name_field = 'Name';


    /**
     * Returns the items for the specified type.
     * @param string item type.
     * @return array item names indexed by item code. The items are order by their position values.
     * An empty array is returned if the item type does not exist.
     */
    public  function ListItems()
    {
        $this->loadItems();
        return $this->_list;
    }

    /**
     * Returns the items for the specified type.
     * @param string item type.
     * @return array item names indexed by item code. The items are order by their position values.
     * An empty array is returned if the item type does not exist.
     */
    public  function lookupItems()
    {
        $this->loadItems();
        return $this->_items;
    }

    /**
     * Returns the item name for the specified type and code.
     * @param string the item type .
     * @param integer the item code (corresponding to the 'code' column value)
     * @return string the item name for the specified the code. False is returned if the item type or code does not exist.
     */
    public  function item($code)
    {
        $this->loadItems();
        return isset($this->_items[$code]) ? $this->_items[$code] : false;
    }

    /**
     * Loads the lookup items for the specified type from the database.
     * @param string the item type
     */
    private function loadItems()
    {
        $id = $this->id_field;
        $name = $this->name_field;
        #$this->_items = array(''=>'');
        $models=$this->owner->model()->findAll();
        foreach($models as $model){
            $this->_items[$model->$id]=$model->$name;
            $this->_list[$model->$name]=$model->$name;
        }

    }
}