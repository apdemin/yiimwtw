<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
	/**
	 * @var string the default layout for the controller view. Defaults to 'column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout='column1';
	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu=array();
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs=array();
    public function  init(){


        if(isset($_GET['lang'])) {
            Yii::app()->language = $_GET['lang'];
            Yii::app()->user->setState('lang', $_GET['lang']);
            $cookie = new CHttpCookie('lang', $_GET['lang']);
            $cookie->expire = time() + (60*60*24*365);
            Yii::app()->request->cookies['lang'] = $cookie;
        }
        else if (Yii::app()->user->hasState('lang'))
            Yii::app()->language = Yii::app()->user->getState('lang');
        else if(isset(Yii::app()->request->cookies['lang']))
            Yii::app()->language = Yii::app()->request->cookies['lang']->value;
		else{
			Yii::app()->language = 'ru';
		}

        $menuItems = Post::model()->findAllByAttributes(array('is_single_page'=>1));
        if($menuItems){
            foreach ($menuItems as $item) {
                /**
                 * @var Post $item;
                 */
                $this->menu[] = array('label'=>$item->title,'url'=>$item->url);
            }
        }


    }
}