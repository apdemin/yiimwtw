<?php

/**
 * This is the model class for table "{{post}}".
 *
 * The followings are the available columns in table '{{post}}':
 * @property integer $id
 * @property string $tags
 * @property integer $status
 * @property string $create_time
 * @property string $update_time
 * @property string $date_active_from
 * @property integer $author_id
 * @property integer $gallery_id
 * @property integer $is_single_page
 * @property integer $is_gallery
 *
 * The followings are the available model relations:
 * @property User $author
 * @property PostLocalization[] $postLocalizations
 *
 *
 * @property string $url
 * @property string $galleryUrl
*/


class Post extends CActiveRecord
{
    const STATUS_DRAFT=1;
    const STATUS_PUBLISHED=2;
    const STATUS_ARCHIVED=3;
    const IMG_SMALL_WIDTH = 160;
    const IMG_SMALL_HEIGHT = 120;

    const DATE_FORMAT = 'Y-m-d H:i';
    const DATETIME_FORMAT = 'Y-m-d H:i:s';

    private $_oldTags;
    public $title;
    public $preview;
    public $content;



    /**
     * @return array
     */
    public static function getStatusList(){
        $arStatus = Lookup::items('PostStatus');
        foreach($arStatus as $key=>$item){
            $arStatus[$key] = Yii::t('db',$item);
        };
        return $arStatus;
    }

    public static function getStatus($status){
        return Yii::t('db',Lookup::item('PostStatus',$status));
    }

    /**
     * @param $code Языковой код
     * @return PostLocalization
     */
    public function getLocalization($code){
        return PostLocalization::model()->findByAttributes(array('post_id'=>$this->id,'lang_id'=>Lang::getIdByCode($code)));
    }
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{post}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('status, date_active_from, is_single_page', 'required'),
			array('status, author_id, gallery_id, is_single_page, is_gallery', 'numerical', 'integerOnly'=>true),
			array('tags', 'safe'),
            array('tags', 'match', 'pattern'=>'/^[\x{0400}-\x{04FF}A-Za-z]+/ui', 'message'=>'Теги могут содержать только буквы'),
            array('status', 'in', 'range'=>array(1,2,3)),
            // The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('title,id, tags, status, create_time, update_time, date_active_from, author_id, is_single_page, is_gallery', 'safe', 'on'=>'search'),
		);
	}


	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'author' => array(self::BELONGS_TO, 'User', 'author_id'),
			'postLocalizations' => array(self::HAS_MANY, 'PostLocalization', 'post_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
            'id' => Yii::t('db','id'),
            'tags' => Yii::t('db','tags'),
            'status' => Yii::t('db','status'),
            'create_time' => Yii::t('db','create_time'),
            'update_time' => Yii::t('db','update_time'),
            'author_id' => Yii::t('db','author_id'),
            'gallery_id' => Yii::t('db','gallery_id'),
            'date_active_from' => Yii::t('db','date_active_from'),
            'is_single_page' => Yii::t('db','is_single_page'),
            'is_gallery' => Yii::t('db','is_gallery'),
            /*search attr*/
            'title' => Yii::t('db','title'),
		);
	}

    /**
     * @return string the URL that shows the detail of the post
     */
    public function getUrl()
    {
        if($this->is_gallery){
            return $this->galleryUrl;
        }else{
            return Yii::app()->createUrl('post/view', array(
                'id'=>$this->id,
            ));
        }
    }
    /**
     * @return string the URL that shows the detail of the post
     */
    public function getGalleryUrl()
    {
        return Yii::app()->createUrl('gallery/view', array(
            'id'=>$this->id,
        ));
    }

    /**
     * @return array a list of links that point to the post list filtered by every tag of this post
     */
    public function getTagLinks()
    {
        $links=array();
        foreach(Tag::string2array($this->tags) as $tag)
            $links[]=CHtml::link(CHtml::encode($tag), array('post/index', 'tag'=>$tag));
        return $links;
    }

    /**
     * Normalizes the user-entered tags.
     */
    public function normalizeTags($attribute,$params)
    {
        $this->tags=Tag::array2string(array_unique(Tag::string2array($this->tags)));
    }


    /**
     * This is invoked when a record is populated with data from a find() call.
     */
    protected function afterFind()
    {
        parent::afterFind();
        $localization = $this->getLocalization(Yii::app()->getLanguage());
        if($localization){
            $this->title = $localization->title;
            $this->preview = $localization->preview;
            $this->content = $localization->content;
        }
        $this->_oldTags=$this->tags;
    }

    /**
     * This is invoked before the record is saved.
     * @return boolean whether the record should be saved.
     */
    protected function beforeSave()
    {
        if(parent::beforeSave())
        {
            if($this->isNewRecord)
            {
                $this->create_time=$this->update_time=date(Post::DATE_FORMAT);
                $this->author_id=Yii::app()->user->id;
            }
            $this->update_time=date(Post::DATE_FORMAT);
            return true;
        }
        else
            return false;
    }

    /**
     * This is invoked after the record is saved.
     */
    protected function afterSave()
    {
        parent::afterSave();
        Tag::model()->updateFrequency($this->_oldTags, $this->tags);
    }

    /**
     * This is invoked after the record is deleted.
     */
    protected function afterDelete()
    {
        parent::afterDelete();
        Tag::model()->updateFrequency($this->tags, '');
    }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.
		$criteria=new CDbCriteria;
        $criteria->together = true;
        $criteria->with = array('postLocalizations'=>array('condition'=>'lang_id='.Lang::getIdByCode('ru')));
		$criteria->compare('id',$this->id);
		$criteria->compare('tags',$this->tags,true);
		$criteria->compare('status',$this->status);

        if(!empty($this->create_time)){
            list($c_date_from,$c_date_to) = explode(' - ',$this->create_time);
            $criteria->addBetweenCondition('create_time', ''.date(Post::DATETIME_FORMAT,strtotime($c_date_from)).'', ''.date(Post::DATETIME_FORMAT,strtotime($c_date_to)).'');
        }
        if(!empty($this->update_time)){
            list($u_date_from,$u_date_to) = explode(' - ',$this->update_time);
            $criteria->addBetweenCondition('update_time', ''.date(Post::DATETIME_FORMAT,strtotime($u_date_from)).'', ''.date(Post::DATETIME_FORMAT,strtotime($u_date_to)).'');
        }
		$criteria->compare('date_active_from',$this->date_active_from,true);
		$criteria->compare('author_id',$this->author_id);
		$criteria->compare('postLocalizations.title',$this->title,true);
		$criteria->compare('is_single_page',$this->is_single_page);
		$criteria->compare('is_gallery',$this->is_gallery);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Post the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


    public function behaviors()
    {
        return array(
            'galleryBehavior' => array(
                'class' => 'GalleryBehavior',
                'idAttribute' => 'gallery_id',
                'versions' => array(
                    'small' => array(
                        'cfit' => array(self::IMG_SMALL_WIDTH, self::IMG_SMALL_HEIGHT),
                    ),
                    'medium' => array(
                        'resize' => array(800, null),
                    )
                ),
                'name' => true,
                'description' => true,
            )
        );
    }
}
