<?php
/**
 * Created by IntelliJ IDEA.
 * User: Demin
 * Date: 08.08.13
 * Time: 23:23
 * To change this template use File | Settings | File Templates.
 */
require_once 'PostController.php';
class GalleryController  extends PostController {

    /**
     * @param CDbCriteria $criteria
     * @return mixed
     */
    protected function loadCriteria($criteria){
        $criteria->addCondition('is_gallery = 1');
        return $criteria;
    }
}