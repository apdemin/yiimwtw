<?php

class PostController extends Controller
{
	public $layout='main';

    /**
     * @param $file CUploadedFile
     * @return string[] url to uploaded file and file name to insert in redactor by default
     * @throws CException
     */
    public function save($file)
    {
        $webroot = Yii::getPathOfAlias('webroot');
        $dstDir = '/uploads/';

        if (!is_dir($webroot . $dstDir)) {
            mkdir($webroot . $dstDir, 0755, true);
        }

        $ext = $file->getExtensionName();
        $name = $file->name;
        if (strlen($ext)) $name = substr($name, 0, -1 - strlen($ext));

        for ($i = 1, $filePath = $dstDir . $name . '.' . $ext; file_exists($webroot . $filePath); $i++) {
            $filePath = $dstDir . $name . " ($i)." . $ext;
        }

        $file->saveAs($webroot . $filePath);
        return array($filePath, $file->name);
    }

    public function actions()
    {
        return array(
            'imgUpload' => array(
                'class' => 'admin.components.RedactorUploadAction',
                'saveCallback' => array($this, 'save'),
                'validator' => array(
                    'mimeTypes' => array('image/png', 'image/jpg', 'image/gif', 'image/jpeg', 'image/pjpeg'),
                )
            ),
        );
    }

	/**
	 * @var CActiveRecord the currently loaded data model instance.
	 */
	private $_model;

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow authenticated users to access all actions
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}



	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Post;
        $arLocalization = array();
        /**
         * @var Lang $lang;
         * @var PostLocalization[] $arLocalization
         */
        foreach (Lang::model()->findAll() as $lang) {
            $localization = new PostLocalization();
            $localization->lang_id = $lang->id;
            $arLocalization[] = $localization;
        }

        if(isset($_POST['Post']))
		{

			$model->attributes=$_POST['Post'];
            $valid = $model->validate();
            foreach ($arLocalization as $localization) {
            $localization->attributes = $_POST['PostLocalization'][$localization->lang->code];
            $valid = $valid && $localization->validate();
        }

            if($valid){
                $model->save();
                foreach ($arLocalization as $localization) {
                    $localization->post_id =  $model->id;
                    $localization->save();
                }
                $this->redirect($this->createUrl('post/list'));

            }
		}

		$this->render('create',array(
			'model'=>$model,
            'arLocalization'=>$arLocalization
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionUpdate()
	{

		$model=$this->loadModel();
        $arLocalization = $model->postLocalizations;


        if(isset($_POST['Post']))
        {

            $model->attributes=$_POST['Post'];
            $valid = $model->validate();
            foreach ($arLocalization as $localization) {
                $localization->attributes = $_POST['PostLocalization'][$localization->lang->code];
                $valid = $valid && $localization->validate();
            }

            if($valid){
                $model->save();
                foreach ($arLocalization as $localization) {
                    $localization->save();
                }
                $this->redirect($this->createUrl('post/list'));

            }
        }

        $this->render('update',array(
            'model'=>$model,
            'arLocalization'=>$arLocalization
        ));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 */
	public function actionDelete()
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel()->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect($this->createUrl('post/list'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
        $this->redirect($this->createUrl('post/list'));
	}

	/**
	 * Manages all models.
	 */
	public function actionList()
	{

		$model=new Post('search');
		if(isset($_GET['Post'])){
            $model->attributes=$_GET['Post'];
        }else{
            $model->is_gallery = null;
            $model->is_single_page = null;
        }
		$this->render('list',array(
			'model'=>$model,
		));
	}

	/**
	 * Suggests tags based on the current user input.
	 * This is called via AJAX when the user is entering the tags input.
	 */
	public function actionSuggestTags()
	{
		if(isset($_GET['q']) && ($keyword=trim($_GET['q']))!=='')
		{
			$tags=Tag::model()->suggestTags($keyword);
			if($tags!==array())
				echo implode("\n",$tags);
		}
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
     * @return Post
	 */
	public function loadModel()
	{
		if($this->_model===null)
		{
			if(isset($_GET['id']))
			{
				if(Yii::app()->user->isGuest)
					$condition='status='.Post::STATUS_PUBLISHED.' OR status='.Post::STATUS_ARCHIVED;
				else
					$condition='';
				$this->_model=Post::model()->findByPk($_GET['id'], $condition);
			}
			if($this->_model===null)
				throw new CHttpException(404,'The requested page does not exist.');
		}
		return $this->_model;
	}


}
