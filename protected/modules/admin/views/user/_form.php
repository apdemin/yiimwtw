<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */
?>

<div class="form">

<?php


/** @var TbActiveForm $form */
$form = $this->beginWidget(
    'booster.widgets.TbActiveForm',
    array(
        'id' => 'user-form',
        'enableAjaxValidation'=>true,
        'htmlOptions' => array('class' => 'well col-md-6'), // for inset effect
    )
);
?>

	<p class="note"><?php echo Yii::t('main','required1')?> <span class="required">*</span> <?php echo Yii::t('main','required2')?></p>

	<div >
		<?php echo $form->textFieldGroup($model, 'username'); ?>
	</div>

	<div >
		<?php echo $form->passwordFieldGroup($model, 'password',array(
            'widgetOptions' => array(
                'htmlOptions' => array('value' => ''),
            )
        ));?>
	</div>

	<div >
		<?php echo $form->passwordFieldGroup($model, 'password_repeat'); ?>
	</div>

	<div >
		<?php echo $form->textFieldGroup($model, 'email');; ?>
	</div>


	<div class=".btn">
		<?php
        $this->widget(
            'booster.widgets.TbButton',
            array('buttonType' => 'submit', 'label' => $model->isNewRecord ? 'Создать' : 'Сохранить')
        );
        ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->