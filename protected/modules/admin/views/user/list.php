<?php
/* @var $this UserController */
/* @var $model User */


$this->setPageTitle( Yii::t('main','Manage Users'));
?>

<h1><?php echo Yii::t('main','Manage Users')?> </h1>
<br>
    <?php echo CHtml::link('Создать пользователя',$this->createUrl('user/create'),array('class' => 'btn btn-large btn-success'));?>

<?php
    $this->widget(
        'booster.widgets.TbGridView',
        array(
            'dataProvider' => $model->search(),
            'type' => 'striped',
            'columns' => array(
                'username',
                'email',
                array(
                    'htmlOptions' => array('nowrap'=>'nowrap'),
                    'class'=>'booster.widgets.TbButtonColumn',
                    'template'=> '{update} {delete}',
                )
            ),
        )
    );
?>
