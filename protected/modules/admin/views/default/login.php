<?php
/**
 * @var DefaultController $this
 * @var LoginForm $model
 */
$this->pageTitle = 'Вход в систему управления сайтом';
?>

<div class="container-fluid">
    <div class="row" style="padding-top: 150px">
        <?php
        /** @var TbActiveForm $form */
        $form = $this->beginWidget(
            'booster.widgets.TbActiveForm',
            array(
                'id' => 'verticalForm',
                'htmlOptions' => array(), // for inset effect
            )
        );
        ?>
        <fieldset class="col-md-4 col-md-offset-4">
            <legend>Форма авторизации</legend>

            <?php
            echo $form->textFieldGroup($model, 'username');
            echo $form->passwordFieldGroup($model, 'password');
            echo $form->checkboxGroup($model, 'rememberMe');
            $this->widget(
                'booster.widgets.TbButton',
                array('buttonType' => 'submit', 'label' => 'Вход','context'=>'success')
            );
            ?>
        </fieldset>
        <?php
        $this->endWidget();
        unset($form);
        ?>
    </div>
</div>


