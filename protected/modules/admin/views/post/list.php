<?php
/**
 * @var Post $model;
 * @var PostController $this;
 */
$this->breadcrumbs = array(
    Yii::t('main','manage_posts')
);
$this->setPageTitle( Yii::t('main','manage_posts'));
?>
<h1><?php echo Yii::t('main','manage_posts');?></h1>
        <br>
        <?php echo CHtml::link( Yii::t('main','Create Post'),$this->createUrl('post/create'),array('class' => 'btn btn-large btn-success'));?>

<?php
$drpOptions = array(
    'locale' => array(
        'applyLabel' => Yii::t('daterangepicker','applyLabel'),
        'cancelLabel' => Yii::t('daterangepicker','cancelLabel'),
        'fromLabel' =>Yii::t('daterangepicker','fromLabel'),
        'toLabel' => Yii::t('daterangepicker','toLabel'),
        'customRangeLabel' => Yii::t('daterangepicker','customRangeLabel'),
    ),
    'format' => 'YYYY-MM-DD HH:mm',
    'weekStart' => '1',
    'timePicker' => true,
    'timePicker12Hour' => false,
    'autoclose' => true,
    'ranges' => array(
        Yii::t('daterangepicker','today') => array('js: moment().subtract(\'days\', 0).hour(0).minute(0), moment().subtract(\'days\', 0).hour(23).minute(59)'),
        Yii::t('daterangepicker','yesterday') => array('js: moment().subtract(\'days\', 1).hour(0).minute(0), moment().subtract(\'days\', 1).hour(23).minute(59)'),
        Yii::t('daterangepicker','thisWeek') => array('js: moment().startOf(\'week\'), moment().endOf(\'week\')'),
        Yii::t('daterangepicker','thisMonth') => array('js: moment().startOf(\'month\'), moment().endOf(\'month\')'),
        Yii::t('daterangepicker','thisYear') => array('js: moment().startOf(\'year\'), moment().endOf(\'year\')'),
    ),
);
$drpCallback = 'function(start, end) {
                                $("#Post_create_time").trigger("change");
                            }';
$clear_dtpicker =  "
    $('#Post_create_time').on('cancel.daterangepicker', function(ev, picker) {
      $('#Post_create_time').val('');
    });
    $('#Post_update_time').on('cancel.daterangepicker', function(ev, picker) {
      $('#Post_update_time').val('');
    });
";
Yii::app()->clientScript->registerScript('clear_dtpicker',$clear_dtpicker );

    $this->widget(
        'booster.widgets.TbGridView',
        array(
            'afterAjaxUpdate' => 'js: function (id, data) {
                $("#Post_create_time").daterangepicker('.CJavaScript::encode($drpOptions).', '.$drpCallback.');
                $("#Post_update_time").daterangepicker('.CJavaScript::encode($drpOptions).', '.$drpCallback.');
                '.$clear_dtpicker.'
            }',
            'type' => 'striped',
            'dataProvider' => $model->search(),
            'template' => "{items}",
            'filter' => $model,
            'id'=>'post_list',
            'columns' => array(
                array(
                    'name'=> 'status',
                    'value'=>'Post::getStatus($data->status)',
                    'filter'=>Post::getStatusList(),
                ),
                array(
                    'name'=> 'create_time',
                    'value'=>'date(Post::DATE_FORMAT,strtotime($data->create_time))',
                    'filter'=>$this->widget('booster.widgets.TbDateRangePicker',
                        array(
                            'model'=>$model,
                            'attribute'=>'create_time',
                            'options' => $drpOptions,
                            'callback' => new CJavaScriptExpression($drpCallback),
                            'htmlOptions' => array(
                                'autocomplete' => 'off',
                               // 'readonly' => 'readonly',
                            ),
                        ),
                        true
                    ),
                ),
                array(
                    'name'=> 'update_time',
                    'value'=>'date(Post::DATE_FORMAT,strtotime($data->update_time))',
                    'filter'=>$this->widget('booster.widgets.TbDateRangePicker',
                        array(
                            'model'=>$model,
                            'attribute'=>'update_time',
                            'options' => $drpOptions,
                            'callback' => new CJavaScriptExpression($drpCallback),
                            'htmlOptions' => array(
                                'autocomplete' => 'off',
                               // 'readonly' => 'readonly',
                            ),
                        ),
                        true
                    ),
                ),
                array(
                    'name'=> 'author_id',
                    'value'=>'$data->author->username',
                    'filter'=>User::getList(),
                ),
                array(
                    'name'=> 'title',
                    'value'=>'$data->getLocalization(\'ru\')->title',
                ),
                array(
                    'name'=> 'is_single_page',
                    'value'=>'($data->is_single_page)?"Да":"Нет"',
                    'filter'=>[1=>"Да",0=>"Нет"],
                    'htmlOptions' => array('style' => 'width:100px'),
//                    'filter'=>'<input type="checkbox" name="Post[is_single_page]" value="1" '.(($model->is_single_page)?'checked':'').'>'
                ),
                array(
                    'name'=> 'is_gallery',
                    'value'=>'($data->is_gallery)?"Да":"Нет"',
                    'filter'=>[1=>"Да",0=>"Нет"],
                    'htmlOptions' => array('style' => 'width:100px'),
//                    'filter'=>'<input type="checkbox" name="Post[is_gallery]" value="1" '.(($model->is_gallery)?'checked':'').'>'
                ),
                array(
                    'htmlOptions' => array('nowrap'=>'nowrap'),
                    'class'=>'booster.widgets.TbButtonColumn',
                    'template'=> '{update} {delete}',
                )
            ),
        )
    );
?>


