<?php
/* @var $this PostController */
/* @var $model Post */
/* @var $arLocalization PostLocalization[] */
$this->breadcrumbs = array(
    Yii::t('main','manage_posts')=>array('list'),
    Yii::t('main','Create Post')
);
?>
<h1><?php echo Yii::t('main','Create Post')?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model,'arLocalization'=>$arLocalization)); ?>