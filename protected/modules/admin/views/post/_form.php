<?
/**
* @var $this PostController
* @var $model Post
* @var $arLocalization PostLocalization[]
 */
?>
<div class="form">

    <?php
    /** @var TbActiveForm $form */
    $form = $this->beginWidget(
        'booster.widgets.TbActiveForm',
        array(
            'id' => 'post-form',
            'htmlOptions' => array('class' => 'well'), // for inset effect
        )
    );


    ob_start();
    echo "<br>";
    echo $form->select2Group(
        $model,
        'tags',
        array(
            'widgetOptions' => array(
                'asDropDownList' => false,
                'options' => array(
                    'tags' => Tag::model()->getAll(),
                    'placeholder' => Yii::t('main','Tags'),
                    'width' => '50%',
                    'tokenSeparators' => array(',', ' ')
                )
            )
        )
    );
    echo $form->dropDownListGroup(
        $model,
        'status',
        array(
            'wrapperHtmlOptions' => array(
                'width' => '50%',
            ),
            'widgetOptions' => array(
                'data' =>Post::getStatusList(),
                'htmlOptions' => array('style'=>'width:50%'),
            )
        )
    );

    echo $form->dateTimePickerGroup(
        $model,
        'date_active_from',
        array(
            'groupOptions' => array(
                'style'=>'width:50%'
            ),
            'widgetOptions' => array(
                'options'=>array(
                    'format' => 'yyyy-mm-dd hh:ii',
                ),
            ),
            'append' => '<i class="glyphicon glyphicon-calendar"></i>'
        )
    );

    echo $form->checkboxGroup(
        $model,
        'is_single_page',
        array(
            'groupOptions' => array(
                'style'=>'width:50%'
            ),
        )
    );

    echo $form->checkboxGroup(
        $model,
        'is_gallery',
        array(
            'groupOptions' => array(
                'style'=>'width:50%'
            ),
        )
    );
    $general = ob_get_clean();

    $arTabs = array(
        array('label' => Yii::t('main','General'), 'content' => $general,'active' => true),
    );

    $arErrorsTab = array();
    foreach ($arLocalization as $localization) {
        ob_start();
        echo "<br>";

        $code = $localization->lang->code;

        echo $form->textFieldGroup(
            $localization,
            "[$code]title",
            array(
                'groupOptions' => array(
                    'style'=>'width:50%'
                ),
                'widgetOptions'=>array(
                    'htmlOptions' => array(
                        'placeholder'=>$localization->getAttributeLabel('title')
                    )
                )
            )
        );

        echo $form->redactorGroup(
            $localization,
            "[$code]preview",
            array(
                'widgetOptions' => array(
                    'editorOptions' =>array(
                        'class' => 'span4',
                        'rows' => 15,
                        'plugins' => array('fullscreen'),
                        'options' => array(
                            'lang' => 'ru',

                        ),
                        'imageUpload'=>$this->createUrl('imgUpload'), // адрес действия на сервере
                        'imageUploadErrorCallback'=>'js:function(obj, json){ alert(json.error); }', // функция для отображения ошибок загрузки пользователю
                    )
                )
            )
        );
        echo $form->redactorGroup(
            $localization,
            "[$code]content",
            array(
                'widgetOptions' => array(
                    'editorOptions' =>array(
                        'class' => 'span4',
                        'rows' => 15,
                        'plugins' => array('fullscreen'),
                        'options' => array(
                            'lang' => 'ru'
                        ),
                        'imageUpload'=>$this->createUrl('imgUpload'), // адрес действия на сервере
                        'imageUploadErrorCallback'=>'js:function(obj, json){ alert(json.error); }', // функция для отображения ошибок загрузки пользователю
                    )
                )
            )
        );

        if($localization->hasErrors()){
            $arErrorsTab[] =  $localization->lang->name;
        }
        $arTabs[] = array('label' => $localization->lang->name, 'content' => ob_get_clean());
    }
    ob_start();
    if ($model->galleryBehavior->getGallery() === null) {
        echo '<p>Перед добавлений фотографий в галерею, необходимо сохранить пост</p>';
    } else {
        $this->widget('GalleryManager', array(
            'gallery' => $model->galleryBehavior->getGallery(),
            'controllerRoute' => '/admin/gallerymanager'
        ));
    }
    $gallery = ob_get_clean();
    $arTabs[] = array('label' => Yii::t('main','Gallery'), 'content' => $gallery,'active' => false,'linkOptions' => array('id'=>'gallery_tab'));
    Yii::app()->clientScript->registerScript("galleryTab", "
        $( document ).ready(function() {
            if(!$('#Post_is_gallery').prop('checked')){
                $('#gallery_tab').hide();
            }
            $('#Post_is_gallery').click(function () {
                $('#gallery_tab').toggle(this.checked);
            });
        });
    ");
    ?>
    <?if(!empty($arErrorsTab)):?>
    <div class="alert alert-danger">
        Проверьте содержимое на следующих вкладках: <br>
        <strong><?= implode(',',$arErrorsTab)?></strong>
    </div>
    <?endif?>
    <?php
    $this->widget(
        'booster.widgets.TbTabs',
        array(
            'id'=>'tabs',
            'type' => 'tabs', // 'tabs' or 'pills'
            'tabs' => $arTabs
        )
    );



    $this->widget(
        'booster.widgets.TbButton',
        array(
            'buttonType' => 'submit',
            'label' => Yii::t('main','Save'),
            'htmlOptions' => array(
                'class' => 'btn btn-primary '
            )
        )
    );

    $this->endWidget();
    unset($form);
    ?>


</div>
<!-- form -->