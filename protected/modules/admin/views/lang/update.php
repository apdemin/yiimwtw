<?php
/* @var $this LangController */
/* @var $model Lang */
$this->breadcrumbs = array(
    Yii::t('main','Localization')=>array('list'),
    Yii::t('main','Refresh lang')
);
?>

<h1><?php echo Yii::t('main','Lang info').' '.$model->name; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>