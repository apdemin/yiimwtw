<?php
/* @var $this LangController */
/* @var $model Lang */
/* @var $form CActiveForm */
?>

<div class="form">

<?php


/** @var TbActiveForm $form */
$form = $this->beginWidget(
    'booster.widgets.TbActiveForm',
    array(
        'id' => 'lang-form',
        'enableAjaxValidation'=>false,
        'htmlOptions' => array('class' => 'well col-md-6'), // for inset effect
    )
);
?>

	<p class="note"><?php echo Yii::t('main','required1')?> <span class="required">*</span> <?php echo Yii::t('main','required2')?></p>

	<div >
		<?php echo $form->textFieldGroup($model, 'code'); ?>
	</div>

	<div >
		<?php echo $form->textFieldGroup($model, 'name');; ?>
	</div>


	<div class=".btn">
		<?php
        $this->widget(
            'booster.widgets.TbButton',
            array('buttonType' => 'submit', 'label' => Yii::t('main','Save'),
                'htmlOptions'=>array(
                    'class'=> 'btn-primary'
                )
            )
        );
        ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->