<?php
/* @var $this LangController */
/* @var $model Lang */

$this->breadcrumbs = array(
    Yii::t('main','Localization')
);

$this->setPageTitle( Yii::t('main','Manage Langs'));
?>

<h1><?php echo Yii::t('main','Manage Langs')?> </h1>
<br>
    <?php echo CHtml::link('Добавить язык',$this->createUrl('create'),array('class' => 'btn btn-large btn-success'));?>

<?php
    $this->widget(
        'booster.widgets.TbGridView',
        array(
            'dataProvider' => $model->search(),
            'type' => 'striped',
            'columns' => array(
                'name',
                'code',
                array(
                    'htmlOptions' => array('nowrap'=>'nowrap'),
                    'class'=>'booster.widgets.TbButtonColumn',
                    'template'=> '{update} {delete}',
                )
            ),
        )
    );
?>
