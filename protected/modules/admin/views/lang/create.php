<?php
/* @var $this LangController */
/* @var $model Lang */
$this->breadcrumbs = array(
    Yii::t('main','Localization')=>array('list'),
    Yii::t('main','New lang')
);

?>

<h1>Добавить язык</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>