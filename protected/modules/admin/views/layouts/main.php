<?php
/**
 * Created by IntelliJ IDEA.
 * User: admin
 * Date: 08.08.13
 * Time: 16:55
 * To change this template use File | Settings | File Templates.
 */
 ?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title>MWTW Admin Panel</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <meta charset="utf-8">
        <link rel="shortcut icon" href="/favicon.png"/>
        <title><?php echo $this->pageTitle;?></title>
        <style type="text/css">
            .navbar {
                -webkit-border-radius: 0 !important;
                -moz-border-radius: 0 !important;
                border-radius: 0 !important;
            }
        </style>
    </head>
    <body>
    <?php if(!Yii::app()->user->isGuest):?>

        <?php



        $this->widget(
            'booster.widgets.TbNavbar',
            array(
                'brand' => 'MWTW Admin Panel',
                'type' => 'inverse',
                'brandUrl' => '/admin',
                'fixed' => false,
                'fluid' => true,
                'items' => array(
                    array(
                        'class' => 'booster.widgets.TbMenu',
                        'type' => 'navbar',
                        'items' => array(
                            array('label'=>'Записи', 'url'=>array('/admin/post/list')),
//                            array('label'=>'Локализация', 'url'=>array('/admin/lang/list')),
                            array('label'=>'Управление пользователями', 'url'=>array('/admin/user/list')),
                        )
                    ),
                    array(
                        'class' => 'booster.widgets.TbMenu',
                        'type' => 'navbar',
                        'htmlOptions' => array('class' => 'pull-right'),
                        'items' => array(
                            array('label'=>'Выход', 'url'=> array('/admin/default/logout')),
                            array('label'=>'На сайт', 'url'=> Yii::app()->homeUrl),
                        )
                    )
                )
            )
        );





        ?>
    <?php endif?>
    <div class='container-fluid'>
        <div class="row">
            <div class="col-md-10 col-md-offset-1">

                <?php
                    $this->widget(
                        'admin.components.MyBreadcrumbs',
                        array(
                            'homeLink' => array('label' => Yii::t('main','admin'), 'url' => array('/admin')),
                            'links' => $this->breadcrumbs,
                        )
                    );

                    echo $content
                ?>
            </div> <!-- /container -->
        </div>
    </div>
    </body>
</html>