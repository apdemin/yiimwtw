<?php
/**
 * @var $this GalleryManager
 * @var $model GalleryPhoto
 *
 * @author Bogdan Savluk <savluk.bogdan@gmail.com>
 */
?>
<?php echo CHtml::openTag('div', $this->htmlOptions); ?>
    <!-- Gallery Toolbar -->
    <div class="btn-toolbar gform">
        <div class="btn-group">
            <span class="btn btn-success fileinput-button">
                <i class="glyphicon glyphicon-plus glyphicon-white"></i>
                <?php echo Yii::t('galleryManager.main', 'Add…');?>
                <input type="file" name="image" class="afile" accept="image/*" multiple="multiple"/>
            </span>
        </div>

        <div class="btn-group">
            <span class="btn btn-default disabled edit_selected"><i class="glyphicon glyphicon-pencil"></i> <?php echo Yii::t('galleryManager.main', 'Edit');?></span>
            <span class="btn btn-default disabled remove_selected"><i class="glyphicon glyphicon-remove"></i> <?php echo Yii::t('galleryManager.main', 'Remove');?></span>
        </div>
        <div class="btn-group"  data-toggle="buttons">
            <label class="btn btn-primary">
                <input type="checkbox" style="margin: 0;" class="select_all"/>
                <?php echo Yii::t('galleryManager.main', 'Select all');?>
            </label>
        </div>
    </div>
    <hr/>
    <!-- Gallery Photos -->
    <div class="sorter">
        <div class="images"></div>
        <br style="clear: both;"/>
    </div>


    <div class="modal fade editor-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><?php echo Yii::t('galleryManager.main', 'Edit information')?></h4>
                </div>
                <div class="modal-body">
                    <div class="form"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary save-changes">
                        <?php echo Yii::t('galleryManager.main', 'Save changes')?>
                    </button>
                    <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo Yii::t('galleryManager.main', 'Close')?></button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->



    <div class="overlay">
        <div class="overlay-bg">&nbsp;</div>
        <div class="drop-hint">
            <span class="drop-hint-info"><?php echo Yii::t('galleryManager.main', 'Drop Files Here…')?></span>
        </div>
    </div>


    <div class="progress-overlay">
        <div class="overlay-bg">&nbsp;</div>
        <div class="modal show progress-modal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3><?php echo Yii::t('galleryManager.main', 'Uploading images…')?></h3>
                    </div>
                    <div class="modal-body">
                        <div class="progress progress-striped active">
                            <div class="progress-bar upload-progress"  role="progressbar"></div>
                        </div>
                    </div>
                    <div class="modal-footer">
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->


    </div>
<?php echo CHtml::closeTag('div'); ?>