<?php
/**
 * Created by IntelliJ IDEA.
 * User: Anton Demin
 * Date: 08.06.14
 * Time: 1:31
 */

Yii::import('booster.widgets.TbBreadcrumbs');

class MyBreadcrumbs extends TbBreadcrumbs{

    public function run() {

        if (empty($this->links))
            return;

        echo CHtml::openTag($this->tagName, $this->htmlOptions);

        if ($this->homeLink === null) {
            $this->homeLink = CHtml::link(Yii::t('zii', 'Home'), Yii::app()->homeUrl);
        }

        if(is_array($this->homeLink)){
            $label = $this->homeLink['label'];
            $url = $this->homeLink['url'];
            $this->homeLink = CHtml::link($label,CHtml::normalizeUrl($url));
        }

        if ($this->homeLink !== false) {
            // check whether home link is not a link
            $active = (stripos($this->homeLink, '<a') === false) ? ' class="active"' : '';
            echo '<li' . $active . '>' . $this->homeLink . '</li>';
        }

        end($this->links);
        $lastLink = key($this->links);

        foreach ($this->links as $label => $url) {
            if (is_string($label) || is_array($url)) {
                echo '<li>';
                echo strtr($this->activeLinkTemplate, array(
                    '{url}' => CHtml::normalizeUrl($url),
                    '{label}' => $this->encodeLabel ? CHtml::encode($label) : $label,
                ));
            } else {
                echo '<li class="active">';
                echo str_replace('{label}', $this->encodeLabel ? CHtml::encode($url) : $url, $this->inactiveLinkTemplate);
            }

            echo '</li>';
        }

        echo CHtml::closeTag($this->tagName);
    }

}