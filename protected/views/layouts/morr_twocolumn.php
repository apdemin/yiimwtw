<!DOCTYPE html
    PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru" lang="ru">
<head>
    <link rel="icon" type="image/x-icon" href="/favicon.ico"/>
    <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Test</title>
    <link href="import/css/style.css" type="text/css" rel="stylesheet" media="all"/>
    <link rel="stylesheet" href="import/fonts/stylesheet.css" type="text/css" charset="utf-8" />
    <!--[if lt IE 7]>
    <link href="import/css/style_ie6.css" type="text/css" rel="stylesheet" media="all"/>
    <![endif]-->
    <!--[if IE 7]>
    <link href="import/css/style_ie7.css" type="text/css" rel="stylesheet" media="all"/>
    <![endif]-->
    <!--[if IE 8]>
    <link href="import/css/style_ie8.css" type="text/css" rel="stylesheet" media="all"/>
    <![endif]-->
    <!--[if IE 9]>
    <link href="import/css/style_ie9.css" type="text/css" rel="stylesheet" media="all"/>
    <![endif]-->
    <!--[if IE 10]>
    <link href="import/css/style_ie10.css" type="text/css" rel="stylesheet" media="all"/>
    <![endif]-->

    <style>

    </style>

</head>

<body>
<div id="index">

    <div id="index-top" class='center main-width'>
        <div id="main-underlayer" class='wrp center main-width'>
            <div id="content-underlayer" class='wrp'>
                <div class='top border'></div>
                <div class='top left border'></div>
                <div class='top right border'></div>
                <div class='left border'></div>
                <div class='right border'></div>
                <div class='bottom left border'></div>
                <div class='bottom right border'></div>
                <div class='bottom border'></div>
            </div>
            <div id="sidebar-underlayer" class='wrp'>
                <div class='top border'></div>
                <div class='top left border'></div>
                <div class='top right border'></div>
                <div class='left border'></div>
                <div class='right border'></div>
                <div class='bottom left border'></div>
                <div class='bottom right border'></div>
                <div class='bottom border'></div>
            </div>
        </div>
        <div id="maxwidther" class="main-width center">
            <div id="header">
                <a href="/" class='logo'><img src="import/i/shrift.png" alt="На главную"></a>
                <div id='mainmenu'>
                    <ul>
                        <li class='active'><a href="/">Главная</a></li>
                        <li ><a href="/onecolumn.html" >О проекте</a></li>
                        <li><a href="#" >Галерея</a></li>
                        <li><a href="#" >Контакты</a></li>
                        <li><a href="http://forum.gardarike.org/viewforum.php?f=109" >Форум</a></li>
                    </ul>
                </div>
            </div>

            <div id="container">

                <div id="content">
                    <div class="center block-name">Лента</div>
                    Content    <br/>
                    Content    <br/>
                    Content    <br/>
                    Content    <br/>
                    Content    <br/>
                    Content    <br/>
                    Content    <br/>
                    Content    <br/>
                    Content    <br/>
                    Content    <br/>
                    Content    <br/>
                    Content    <br/>
                    Content    <br/>
                    Content    <br/>
                </div>
            </div>
            <div id="sidebar">
                <div class="center block-name">Меню</div>
                Sidebar
            </div>
            <div class='ca'></div>
        </div>
    </div>
    <div id="foot">
        <div id="footwidther" class='main-width'>

        </div>
    </div>
</div>
</body>
</html>
