<!DOCTYPE html
    PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru" lang="ru">
<head>
    <link rel="icon" type="image/x-icon" href="<?php echo Yii::app()->request->baseUrl; ?>/favicon.png"/>
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo Yii::app()->request->baseUrl; ?>/favicon.png"/>
    <?php
        $cs = Yii::app()->getClientScript();
        $cs->registerCoreScript('jquery');
        //$cs->registerScriptFile(Yii::app()->request->baseUrl.'/import/lightbox/js/lightbox-2.6.min.js', CClientScript::POS_END);
    ?>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/import/css/style.css" type="text/css" rel="stylesheet" media="all"/>
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/import/fonts/stylesheet.css" type="text/css" charset="utf-8" />
    <!--[if lt IE 7]>
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/import/css/style_ie6.css" type="text/css" rel="stylesheet" media="all"/>
    <![endif]-->
    <!--[if IE 7]>
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/import/css/style_ie7.css" type="text/css" rel="stylesheet" media="all"/>
    <![endif]-->
    <!--[if IE 8]>
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/import/css/style_ie8.css" type="text/css" rel="stylesheet" media="all"/>
    <![endif]-->
    <!--[if IE 9]>
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/import/css/style_ie9.css" type="text/css" rel="stylesheet" media="all"/>
    <![endif]-->
    <!--[if IE 10]>
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/import/css/style_ie10.css" type="text/css" rel="stylesheet" media="all"/>
    <![endif]-->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/import/lightbox/css/lightbox.css" rel="stylesheet" />

    <style>

    </style>

</head>

<body>
<div id="index">

    <div id="index-top" class='center main-width'>
        <div id="main-underlayer" class='wrp center main-width bg80'>
            <div class='top border'></div>
            <div class='top left border'></div>
            <div class='top right border'></div>
            <div class='left border'></div>
            <div class='right border'></div>
            <div class='bottom left border'></div>
            <div class='bottom right border'></div>
            <div class='bottom border'></div>
        </div>
        <div id="maxwidther" class="main-width center">
            <div id="header">
                <a href="/" class='logo'><img src="<?php echo Yii::app()->request->baseUrl; ?>/import/i/shrift.png" alt="На главную"></a>
                <div id='mainmenu'>
                    <?php $this->widget('zii.widgets.CMenu',array(
                        'items'=>array(
                            array('label'=>Yii::t('main','Home'), 'url'=>array('post/index')),
                            array('label'=>Yii::t('main','About'), 'url'=>array('site/page', 'view'=>'about')),
                            array('label'=>Yii::t('main','Gallery'), 'url'=>array('gallery/list')),
                            array('label'=>Yii::t('main','Contact'), 'url'=>array('site/contact')),
                            array('label'=>Yii::t('main','Forum'), 'url'=>'http://forum.gardarike.org/viewforum.php?f=109'),
                        ),
                    )); ?>
                </div>
            </div>
            <div id="content">


                <?php echo $content; ?>


            </div>

            <div class='ca'></div>
        </div>
    </div>
    <div id="foot">
        <div id="footwidther" class='main-width'>
            <?php echo Yii::powered(); ?>
        </div>
    </div>
</div>
</body>
</html>
