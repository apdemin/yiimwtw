<?php
/**
 * @var Post $model
 * @var Gallery $gallery
 */
//$models = Post::model()->findAll();
//foreach($models as $modelq) $modelq->galleryBehavior->changeConfig();
$this->pageTitle=Yii::app()->name .' - '.Yii::t('main','Gallery').' - '.$model->title;
$gallery = $model->galleryBehavior->getGallery();
$photos = $gallery->galleryPhotos;
?>

<div class="gallery-detail">
    <div class="title">
        <h2><?php echo $model->title?></h2>
    </div>

    <div class="content">
        <?php
        $this->beginWidget('CMarkdown', array('purifyOutput'=>true));
        echo $model->content;
        $this->endWidget();
        ?>
        <br>
        <br>
        <br>
        <?foreach ($photos as $photo):?>
            <a class="gallery_image" href="<?=$photo->getUrl()?>" data-lightbox="gallery" data-title="<?=$photo->description?>">
                <img src="<?=$photo->getUrl('small')?>" alt=""/>
            </a>
        <?endforeach;?>
        <div class="ca"></div>
    </div>
    <div class="nav">
        <?php
        if(!empty($model->tagLinks[0])):
            ?>
            <i><?php echo Yii::t('main','Tags')?></i>:
            <?php echo implode(', ', $model->tagLinks); ?>
            <br/>
        <?php endif;?>
        <hr>
        <span class='date'><?php echo date('d.m.Y',strtotime($model->update_time)); ?></span>
        <?php if(!isset($_GET['id'])):?>
            <span class="more"><?php echo CHtml::link(Yii::t('main','Read more'), $model->url); ?></span>
            <div class="ca"></div>
        <?php endif;?>

    </div>
</div>


