<?
$this->pageTitle=Yii::app()->name .' - '.Yii::t('main','Gallery');
$this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
	'template'=>"{items}\n{pager}",
));
