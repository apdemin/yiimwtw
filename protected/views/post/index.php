<?
$this->pageTitle=Yii::app()->name .' - '.Yii::t('main','News');
if(!empty($_GET['tag'])): ?>
<h3><?php echo Yii::t('main','Posts Tagged with')?> <i><?php echo CHtml::encode($_GET['tag']); ?></i></h3>
<?php endif; ?>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
	'template'=>"{items}\n{pager}",
)); ?>
