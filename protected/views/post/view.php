<?php
/**
 * @var Post $model
 */

$this->pageTitle=$model->title;
?>

<div class="post-detail">
    <div class="title">
        <h2><?php echo $model->title?></h2>
    </div>

    <div class="content">
        <?php
        $this->beginWidget('CMarkdown', array('purifyOutput'=>true));
        echo $model->content;
        $this->endWidget();
        ?>
    </div>
    <div class="nav">
        <?php
        if(!empty($model->tagLinks[0])):
            ?>
            <i><?php echo Yii::t('main','Tags')?></i>:
            <?php echo implode(', ', $model->tagLinks); ?>
            <br/>
        <?php endif;?>
        <hr>
        <span class='date'><?php echo date('d.m.Y',strtotime($model->update_time)); ?></span>
        <?php if(!isset($_GET['id'])):?>
            <span class="more"><?php echo CHtml::link(Yii::t('main','Read more'), $model->url); ?></span>
            <div class="ca"></div>
        <?php endif;?>

    </div>
</div>


