<?
/**
 * @var Post $data
 */
?>
<div class="post">
	<div class="title">
		<h4><?php echo CHtml::link(CHtml::encode($data->title), $data->url); ?></h4>
	</div>

	<div class="content">
		<?php
			$this->beginWidget('CMarkdown', array('purifyOutput'=>true));
			echo $data->preview;
			$this->endWidget();
		?>
	</div>
	<div class="nav">
        <?php
            if(!empty($data->tagLinks[0])):
        ?>
            <i><?php echo Yii::t('main','Tags')?></i>:
            <?php echo implode(', ', $data->tagLinks); ?>
            <br/>
        <?php endif;?>
        <hr>
        <span class='date'><?php echo date('d.m.Y',strtotime($data->update_time)); ?></span>
        <?php if(!isset($_GET['id'])):?>
		    <span class="more"><?php echo CHtml::link(Yii::t('main','Read more'), $data->url); ?></span>
            <div class="ca"></div>
        <?php endif;?>

	</div>
</div>
