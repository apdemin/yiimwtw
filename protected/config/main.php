<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'MWTW',

	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
        'application.modules.admin.components.galleryManager.models.*',
        'application.modules.admin.components.galleryManager.GalleryManager',
        'application.modules.admin.components.galleryManager.GalleryBehavior',
        'application.modules.admin.components.yii-image.*',
		'application.components.*',
	),
    'controllerMap' => array(
        'gallerymanager' => 'admin.components.galleryManager.GalleryController',
    ),

    'modules'=>array(
        'gii'=>array(
            'class'=>'system.gii.GiiModule',
            'password'=>'qweqwe',
            'ipFilters'=>array('127.0.0.1'),
        ),
        'admin'=>array(
            'import' => array(
            ),
            'controllerMap' => array(
                'gallerymanager' => 'admin.components.galleryManager.GalleryController',
            ),
            'components' => array(
                'booster' => array(
                    'class' => 'admin.components.bootstrap.components.Booster'
                ),
                'galleryManager' => array(
                    'class' => 'admin.components.galleryManager.GalleryManager'
                )
            ),
            'preload' => array(
                'booster'
            ),
        ),
    ),

	'defaultController'=>'post',

	// application components
	'components'=>array(
		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
            'loginUrl'=>array('admin/default/login'),
		),
        'assetManager' => array(
            'linkAssets' => false,
        ),
        'image'=>array(
            'class'=>'admin.components.yii-image.CImageComponent',
            // GD or ImageMagick
            'driver'=>'GD',
            // ImageMagick setup path
            //'params'=>array('directory'=>'D:/Program Files/ImageMagick-6.4.8-Q16'),
        ),

		'db'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=mwtw',
			'emulatePrepare' => true,
			'username' => 'root',
			'password' => '',
			'charset' => 'utf8',
			'tablePrefix' => 'tbl_',
		),

		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),
		'urlManager'=>array(
			'urlFormat'=>'path',
            'showScriptName'=>false,
			'rules'=>array(
                '/post/<id:\d+>'=>'post/view',
                '/gallery/<id:\d+>'=>'gallery/view',
                '/user/<id:\d+>'=>'user/view',
                '/posts/<tag:.*?>'=>'post/index',
                '/post/update/<id:\d+>'=>'post/update',
                '/page/<view:\w+>'=>'site/page',
                '<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>require(dirname(__FILE__).'/params.php'),
);