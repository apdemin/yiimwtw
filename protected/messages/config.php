<?php
/**
 * Created by IntelliJ IDEA.
 * User: admin
 * Date: 05.08.13
 * Time: 12:48
 * To change this template use File | Settings | File Templates.
 */


return array(
    'sourcePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'messagePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'messages',
    //'languages' => array('zh_cn', 'zh_tw', 'de', 'el', 'es', 'sv', 'he', 'nl', 'pt', 'pt_br', 'ru', 'it', 'fr', 'ja', 'pl', 'hu', 'ro', 'id', 'vi', 'bg', 'lv', 'sk'),
    'languages' => array('ru'),
    'fileTypes' => array('php'),
    'overwrite' => true,
    'exclude' => array(
        '/messages',
        '/vendors',
        '/assets',
    ),
);