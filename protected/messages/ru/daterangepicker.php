<?php
return array(
    'applyLabel' => 'Применить фильтр',
    'cancelLabel' => 'Очистить',
    'fromLabel' => 'От',
    'toLabel' => 'До',
    'customRangeLabel' => 'Свой диапазон',
    'today' => 'За сегодня',
    'yesterday'=>'За вчера',
    'thisWeek'=>'За эту неделю',
    'thisMonth' => 'За этот месяц',
    'thisYear'=>'За текущий год',
);