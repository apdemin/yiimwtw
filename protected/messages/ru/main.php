<?php
/**
 * Message translations.
 *
 * This file is automatically generated by 'yiic message' command.
 * It contains the localizable messages extracted from source code.
 * You may modify this file by translating the extracted messages.
 *
 * Each array element represents the translation (value) of a message (key).
 * If the value is empty, the message is considered as not translated.
 * Messages that no longer need translation will have their translations
 * enclosed between a pair of '@@' marks.
 *
 * Message string can be used with plural forms format. Check i18n section
 * of the guide for details.
 *
 * NOTE, this file must be saved in UTF-8 encoding.
 *
 * @version $Id: $
 */
return array(
    /*main*/
    'About' => 'О проекте',
    'Contact' => 'Контакты',
    'Gallery' => 'Галерея',
    'Forum' => 'Форум',
    'Home' => 'Главная',
    'News' => 'Новости',
    'Login' => 'Вход',
    'Logout' => 'Выход',
    'Error' => 'Ошибка',
    'Read more' => 'Читать далее...',
    'AppName' => 'Демо блог Yii',
    'Posts Tagged with' => 'Записи с тегом ',
    'username' => 'Логин',
    'password' => 'Пароль',
    'rememberMe' => 'Запомнить меня',

    /*posts*/
    'manage_posts' => 'Управление записями',
    'Create New Post' => 'Создать новую запись',
    'Create Post' => 'Создать запись',
    'Update Post' => 'Обновить запись',
    'Tags' => 'Теги',
    'Menu' => 'Меню',
    'Save' => 'Сохранить',

    'name' => 'Имя',
    'email' => 'email',
    'subject' => 'Тема',
    'body' => 'Текст письма',
    'verifyCode' => 'Проверочный код',
    'mailSubmit' => 'Отправить',
    'required1' => 'Поля, помеченные',
    'required2' => ', обязательны к заполнению',
    'contactText' => 'Вы можете связаться с разработчиками следующими способами:',
    /*users*/
    'Users' => 'Пользователи',
    'Manage Users' => 'Управление пользователями',
    'List User' => 'Список пользователей',
    'Create User' => 'Создать пользователя',
    'allowed' => 'Доступные действия',
    'Manage Gallery' => 'Управление галереями',
    'Manage Langs' => 'Управление языками',
    'admin' => 'Панель управления',
    'Localization' => 'Локализация',
    'Refresh lang' => 'Обновить язык',
    'Lang info' => 'Сведения о языке',
    'New lang' => 'Добавить язык',
    'General' => 'Основное',

    /*gallery*/
    'Add Gallery' => 'Добавить галерею',
    'manage_gallery' => 'Управление галереями',


);