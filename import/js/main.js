/**
 * Created with IntelliJ IDEA.
 * User: Demin
 * Date: 01.03.15
 * Time: 13:56
 * To change this template use File | Settings | File Templates.
 */
$( document ).ready(function() {
    $('.items .content a:has(img)').each(function() {
        $(this).attr('data-lightbox','items-gallery');
    });
    $('.post-detail .content a:has(img)').each(function() {
        $(this).attr('data-lightbox','post-gallery');
    });

});